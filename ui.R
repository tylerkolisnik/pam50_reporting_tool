fluidPage(
  titlePanel("NRG-GU006 PAM50 Reporting Tool",windowTitle="NRG-GU006 PAM50 Reporting Tool"),
  mainPanel(
    img(src="gdxlogo.png"),
    hr(),
    img(src='RUO.png'),
    
  # Copy the line below to make a text input box
  textInput("text", label = h3("Enter Celfile Name"), value = ""),
  
  hr(),
  h3("Celfile Name"),
  verbatimTextOutput("v1"),
  h3("PAM50 Subtype"),
  verbatimTextOutput("v2"),
  h3("PAM50 Basal Score"),
  verbatimTextOutput("v3"),
  h3("PAM50 Luminal A Score"),
  verbatimTextOutput("v4"),
  h3("PAM50 Luminal B Score"),
  verbatimTextOutput("v5"),
  downloadButton("downloadData", "Download"),
  hr(),
  h4("Supported by: Data and Quality Control Management"),
  h4("1038 Homer Street"),
  h4("Vancouver, BC V6B 2W9"),
  h4("dataqc@genomedx.com | www.genomedx.com")
  ) 

)